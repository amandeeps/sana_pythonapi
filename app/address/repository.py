from flask import Flask, jsonify, json, request, abort, render_template, Blueprint
from .models import Address
from .modelsContact import AddressContact
from ..database import session_scope

class AddressRepository:
    
	
    def getBillingAddressByOrderNo(orderNo):
        try:
            with session_scope() as session:      

                    address = session.query(Address, AddressContact) \
                              .filter(Address.link_no == orderNo) \
                              .filter(Address.link_table == 'SORD') \
                              .filter(Address.addr_type == 'B') \
                              .filter(Address.id == AddressContact.address_id) \
                              .filter(AddressContact.contact_type_id == 0) \
                              .first()
                    return address

        except Exception as ex:
           raise Exception(str(ex))
        
        return None

    def getShippingAddressByOrderNo(orderNo):
        try:
            with session_scope() as session:      

                    address = session.query(Address, AddressContact) \
                                         .filter(Address.link_no == orderNo) \
                                         .filter(Address.link_table == 'SORD') \
                                         .filter(Address.addr_type == 'S') \
                                         .filter(Address.id == AddressContact.address_id) \
                                         .filter(AddressContact.contact_type_id == 0) \
                                         .first()
                    return address

        except Exception as ex:
           raise Exception(str(ex))
        
        return None

    def getEmailByCustomerNo(customerNo):
        try:
            with session_scope() as session:      

                    address = session.query(Address, AddressContact) \
                              .filter(Address.link_no == customerNo) \
                              .filter(Address.link_table == 'CUST') \
                              .filter(Address.addr_type == 'B') \
                              .filter(Address.id == AddressContact.address_id) \
							  .filter(AddressContact.contact_type_id == 0) \
                              .first()
                    return address.AddressContact.email

        except Exception as ex:
           raise Exception(str(ex))
        
        return None

    def getCustomerBillingAddress(customerNo):
        try:
            with session_scope() as session:      

                    return session.query(Address, AddressContact) \
                              .filter(Address.link_no == customerNo) \
                              .filter(Address.link_table == 'CUST') \
                              .filter(Address.addr_type == 'B') \
                              .filter(Address.id == AddressContact.address_id) \
                              .filter(AddressContact.contact_type_id == 0) \
                              .first()

        except Exception as ex:
           raise Exception(str(ex))
        
        return None

    def getAllShippingAddressByCustNo(customerNo):
        try:
            with session_scope() as session:      

                   return session.query(Address, AddressContact) \
                                         .filter(Address.link_no == customerNo) \
                                         .filter(Address.link_table == 'CUST') \
                                         .filter(Address.addr_type == 'S') \
                                         .filter(Address.id == AddressContact.address_id) \
                                         .filter(AddressContact.contact_type_id == 0) \
                                         .all()

        except Exception as ex:
           raise Exception(str(ex))
        
        return None

    def getAddressById(id):
        try:
            with session_scope() as session:      

                return session.query(Address, AddressContact)\
                                   .filter(Address.id == id) \
                                   .filter(Address.id == AddressContact.address_id) \
								   .filter(AddressContact.contact_type_id == 0) \
                                   .first()

        except Exception as ex:
            raise Exception(str(ex))
        
        return None

