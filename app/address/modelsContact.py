from sqlalchemy import Boolean, Column, DateTime, ForeignKey, Integer, Numeric, String, Table, Text, text, BigInteger, Date, SmallInteger, UniqueConstraint, Index
from sqlalchemy.dialects.postgresql import UUID, ARRAY, HSTORE, JSONB
from sqlalchemy.orm import relationship
from ..database import Base


class AddressContact(Base):
    __tablename__ = 'address_contacts'

    _dbversion = Column(Integer)
    _modified = Column(DateTime)
    _modified_by = Column(String(3))
    _created = Column(DateTime)
    _created_by = Column(String(3))
    id = Column(Integer, primary_key=True, server_default=text("nextval('address_contacts_id_seq'::regclass)"))
    address_id = Column(Integer, nullable=False)
    contact_type_id = Column(Integer, nullable=False)
    phone = Column(String(30), nullable=False)
    fax = Column(String(30), nullable=False)
    cell = Column(String(30), nullable=False)
    email = Column(String(254), nullable=False)
    name = Column(ARRAY(String(length=80)), nullable=False)

    def __repr__(self):
        return ('<Address({0},{1},{2},{3},{4},{5},{6}>') \
                    .format(self.id, 
                            self.address_id,
                            self.phone, 
                            self.fax,
                            self.cell,
                            self.email,
                            self.name,
							)