from flask import Flask, jsonify, json, request, abort, render_template, Blueprint
from .repository import AddressRepository

class AddressService:

    def getBillingAddressByOrderNo(orderNo):
        try:
            return AddressRepository.getBillingAddressByOrderNo(orderNo)

        except Exception as ex:
            raise Exception(str(ex))
        
        return None

    def getShippingAddressByOrderNo(orderNo):
        try:
            return AddressRepository.getShippingAddressByOrderNo(orderNo)

        except Exception as ex:
            raise Exception(str(ex))
        
        return None
    
    def buildOrderAddress(address):
        try:
            result = {
                "id": address.Address.id,
                "type": address.Address.addr_type,
                "email": address.AddressContact.email,
                "city": address.Address.city,
                "postalCode": address.Address.postal_zip,
                "provState": address.Address.prov_state,
                "country": address.Address.country_code,
                "phone": {
                    "number": address.AddressContact.phone
                },
                "fax": {
                    "number": address.AddressContact.fax
                },
                "line1": address.Address.address[0],
                "line2": address.Address.address[1],
                "line3": address.Address.address[2],
                "line4": address.Address.address[3],
                "created": address.Address._created,
                "modified": address.Address._modified,
                "name": address.Address.name
            }            
            return result

        except Exception as ex:
            raise Exception(str(ex))
        
        return None

    def buildCustomerAddress(address):
        try:
            result = {
                "id": address.Address.id,
                "type": address.Address.addr_type,
                "email": address.AddressContact.email,
                "city": address.Address.city,
                "postalCode": address.Address.postal_zip,
                "provState": address.Address.prov_state,
                "country": address.Address.country_code,
                "phone": {
                    "number": address.AddressContact.phone
                },
                "fax": {
                    "number": address.AddressContact.fax
                },
                "line1": address.Address.address[0],
                "line2": address.Address.address[1],
                "line3": address.Address.address[2],
                "line4": address.Address.address[3],
                "created": address.Address._created,
                "modified": address.Address._modified,
                "name": address.Address.name,
                "shipId": address.Address.ship_id,
                "sellLevel": address.Address.sell_no,
                "contacts": AddressService.buildCustomerContacts(address)
            }            
            return result

        except Exception as ex:
            raise Exception(str(ex))        
        return None

    def buildCustomerBillingAddress(address):
       return AddressService.buildCustomerAddress(address)

    def buildCustomerShippingAddresses(shipping_addresses):
        try:
            result = []  
            
            for address in shipping_addresses:
                result.append(AddressService.buildCustomerAddress(address))        
            
            return result

        except Exception as ex:
            raise Exception(str(ex))
        
        return None

    def buildCustomerContacts(address):
        try:

            result = []  
            
                result.append({
                "name": address.AddressContact.name,
                "email": address.AddressContact.email,
                    "phone": {
                    "number": address.AddressContact.phone,
                        "format": 1
                    },
                    "fax": {
                    "number": address.AddressContact.fax,
                        "format": 1
                    },
                })        
            
            return result;

        except Exception as ex:
            raise Exception(str(ex))
        
        return None

    def getEmailByCustomerNo(customerNo):
        try:
            return AddressRepository.getEmailByCustomerNo(customerNo)

        except Exception as ex:
            raise Exception(str(ex))
        
        return None

    def getCustomerBillingAddress(customerNo):
        try:
            return AddressRepository.getCustomerBillingAddress(customerNo)

        except Exception as ex:
            raise Exception(str(ex))
        
        return None
    
    def getAllShippingAddressByCustNo(customerNo):
        try:
            return AddressRepository.getAllShippingAddressByCustNo(customerNo)

        except Exception as ex:
            raise Exception(str(ex))
        
        return None

    def getAddressById(id):
        try:
            return AddressRepository.getAddressById(id)
        except Exception as ex:
            raise Exception(str(ex))
    